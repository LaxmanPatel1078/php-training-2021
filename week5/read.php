<?php

session_start();
 
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

?>
<?php
include 'functions.php';
$pdo = pdo_connect_mysql();


$page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
$records_per_page = 5;


$stmt = $pdo->prepare('SELECT * FROM contacts ORDER BY id LIMIT :current_page, :record_per_page');
$stmt->bindValue(':current_page', ($page-1)*$records_per_page, PDO::PARAM_INT);
$stmt->bindValue(':record_per_page', $records_per_page, PDO::PARAM_INT);

$stmt = $pdo->prepare('SELECT * FROM contacts ORDER BY id ');
$stmt->execute();
$contacts = $stmt->fetchAll(PDO::FETCH_ASSOC);

$num_contacts = $pdo->query('SELECT COUNT(*) FROM contacts')->fetchColumn();
?>


<?=template_header('Read')?>

<div class="content read">
	<h2>Student Records</h2> 

	<table>
        <thead>
            <tr>
                <td>#id</td>
                <td>Name</td>
                <td>Email</td>
                <td>Phone</td>
                <td>Class</td>
                <td>Gender</td>
                <td>House</td>
                <td>Vehicle</td>
                <td>Bio</td>
                
                <td>Created</td>
                <td>View Button<td>
               
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contacts as $contact): ?>
            <tr>
                <td><?=$contact['id']?></td>
                <td><?=$contact['name']?></td>
                <td><?=$contact['email']?></td>
                <td><?=$contact['phone']?></td>
                <td><?=$contact['class']?></td>
                <td><?=$contact['gender']?></td>
                <td><?=$contact['house']?></td>
                <td><?=$contact['vehicle1']?><?=" ".$contact['vehicle2']?>
                <?=" ".$contact['vehicle3']?></td>
                <td><?=$contact['bio']?></td>
                <td><?=$contact['created']?></td>
                <td><a href="viewprofile.php?id=<?=$contact['id']?>" title="view">View</a></td>

             
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="pagination">
		<?php if ($page > 1): ?>
		<a href="read.php?page=<?=$page-1?>"><i class="fas fa-angle-double-left fa-sm"></i></a>
		<?php endif; ?>
		<?php if ($page*$records_per_page < $num_contacts): ?>
		<a href="read.php?page=<?=$page+1?>"><i class="fas fa-angle-double-right fa-sm"></i></a>
		<?php endif; ?>
	</div>
	
</div>

<?=template_footer()?>