<?php

session_start();
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
  header("location: login.php");
  exit;
}
 ?>
<?php
include 'functions.php';
$pdo = pdo_connect_mysql();


$name = $_SESSION['username'];
$stmt = $pdo->prepare("SELECT * FROM contacts WHERE username = ? LIMIT 1"); 
$stmt->execute([$name]); 
$contact = $stmt->fetch();

$id = $contact['id'];
$stmt = $pdo->prepare("SELECT * FROM images WHERE id = ? LIMIT 1"); 
$stmt->execute([$id]); 
$image = $stmt->fetch();


?>



<?=template_header('Read')?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <title>Student Profile </title>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet"><link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css'>

   
    <link rel="stylesheet" href="demo.css" />
    <link rel="stylesheet" href="style2.css">
</head>
<body>
		
<pre>

</pre>
         

<div class="student-profile py-4">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <div class="card shadow-sm">
          <div class="card-header bg-transparent text-center">
            <img class="profile_img" src="<?=$image['image']?>" alt="student dp">
            <h3><?=$contact['name']?></h3>
            <a href="upload.php?id=<?=$contact['id']?>" title="Update">Upload</a>
          </div>
          <div class="card-body">
            <p class="mb-0"><strong class="pr-1">Student ID:</strong><?=$contact['id']?></p>
            <p class="mb-0"><strong class="pr-1">Class :</strong><?=$contact['class']?></p>
            <p class="mb-0"><strong class="pr-1">House :</strong><?=$contact['house']?></p>
     

            <div class="ScriptTop">
    <div class="rt-container">
     
        <div class="col-rt-2">
            <ul>
            
            <li><a href="update.php?id=<?=$contact['id']?>" title="Update">EDIT</a></li>
            <li><a href="delete.php?id=<?=$contact['id']?>" title="Update">DELETE</a></li>
            </ul>
        </div>
    </div>
</div>
          </div>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="card shadow-sm">
          <div class="card-header bg-transparent border-0">
            <h3 class="mb-0"><i class="far fa-clone pr-1"></i>General Information</h3>
          </div>
          <div class="card-body pt-0">
            <table class="table table-bordered">
              <tr>
                <th width="30%">Email</th>
                <td width="2%">:</td>
                <td><?=$contact['email']?></td>
              </tr>
              <tr>
                <th width="30%">Phone 	</th>
                <td width="2%">:</td>
                <td><?=$contact['phone']?></td>
              </tr>
              <tr>
                <th width="30%">Gender</th>
                <td width="2%">:</td>
                <td><?=$contact['gender']?></td>
              </tr>
              <tr>
                <th width="30%">House</th>
                <td width="2%">:</td>
                <td><?=$contact['house']?></td>
              </tr>
              <tr>
                <th width="30%">Vehicle </th> 
                <td width="2%">:</td>
                <td><?=$contact['vehicle1']?><?=" ".$contact['vehicle2']?>
                <?=" ".$contact['vehicle3']?></td>
              </tr>
            </table>
          </div>
        </div>
          <div style="height: 26px"></div>
        <div class="card shadow-sm">
          <div class="card-header bg-transparent border-0">
            <h3 class="mb-0"><i class="far fa-clone pr-1"></i>About </h3>
          </div>
          <div class="card-body pt-0">
              <p><?=$contact['bio']?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

           
    		</div>
		</div>
    </div>
</section>
     



	</body>
</html>
<?=template_footer()?>