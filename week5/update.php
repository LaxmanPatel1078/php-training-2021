
<?php

session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
  header("location: login.php");
  exit;
}
 ?>


<?php
include 'functions.php';
$pdo = pdo_connect_mysql();
$msg = '';
if (isset($_GET['id'])) {
    if (!empty($_POST)) {
        $id = $_GET['id'];
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $phone = isset($_POST['phone']) ? $_POST['phone'] : '';
        $class = isset($_POST['class']) ? $_POST['class'] : '';

        $gender = isset($_POST['gender']) ? $_POST['gender'] :'';
        $house = isset($_POST['house']) ? $_POST['house'] : '';
        $bio = isset($_POST['bio']) ? $_POST['bio'] : '';

        $vehicle1 = isset($_POST['vehicle1']) ? $_POST['vehicle1'] : '';
        $vehicle2 = isset($_POST['vehicle2']) ? $_POST['vehicle2'] : '';
        $vehicle3 = isset($_POST['vehicle3']) ? $_POST['vehicle3'] : '';
        $created = date('Y-m-d\TH:i');
       


        $stmt = $pdo->prepare('UPDATE contacts SET id = ?, name = ?, email = ?, phone = ?, class = ?,gender =?,house=?,
        vehicle1= ?,vehicle2= ?,vehicle3= ?,bio = ?, created = ? WHERE id = ?');
        $stmt->execute([$id, $name, $email, $phone, $class,$gender,$house,$vehicle1,$vehicle2,
        $vehicle3,$bio, $created, $_GET['id']]);
        $msg = 'Updated Successfully!';
        header('Location:profile.php');
    }
    $stmt = $pdo->prepare('SELECT * FROM contacts WHERE id = ?');
    $stmt->execute([$_GET['id']]);
    $contact = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$contact) {
        exit('Contact doesn\'t exist with that ID!');
    }
} else {
    exit('No ID specified!');
}
?>
<?=template_header('Read')?>

<div class="content update">
	<h2>Update Details #<?=$contact['id']?></h2>
    <form action="update.php?id=<?=$contact['id']?>" method="post">
    <form action="create.php" method="post">
   
        <label for="name">Name</label>
        <input type="text" name="name" placeholder="John Doe" id="name" required>

        <label for="email">Email</label>
        
        <input type="text" name="email" placeholder="johndoe@example.com" id="email" required>
        <label for="phone">Phone</label>
        <input type="text" name="phone" placeholder="2025550143" id="phone" required>
        <label for="title">Class </label>
        
        <input type="numeric" name="class" placeholder="eg. 12" id="class" required>
        <label for="name">Bio </label>
        <input type="text" name="bio" placeholder="Write something about you" id="bio" required>

  
          <div>
           <p>Please select your gender:</p>
            <hr>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label>
        <hr>
          </div>
    
          <div> 
          <pre>    </pre>
          <label for="house">Choose your House :</label>
      
            <select name="house" id="house" required>
            <option value="apple">apple</option>
            <option value="aryabhatt">aryabhatt</option>
            <option value="bhaskar">bhaskar</option>
            <option value="rohini">Rohini</option>
            </select>
            
          </div>
          

            <div>
            <hr>
            <pre>    </pre>
            <label for="cars">Select vehicle you have at Home :</label>
            <hr>
            <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
            <label for="vehicle1"> I have a bike</label><br>
            <input type="checkbox" id="vehicle2" name="vehicle2" value="Car">
            <label for="vehicle2"> I have a car</label><br>
            <input type="checkbox" id="vehicle3" name="vehicle3" value="Boat">
            <label for="vehicle3"> I have a Bicycle</label><br><br>
            <hr>
            </div>
            <br>
          
          
        <input type="submit" value="update">
    </form>
    <?php if ($msg): ?>
    <p><?=$msg?></p>
    <?php endif; ?>
</div>

<?=template_footer()?>