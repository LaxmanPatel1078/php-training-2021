<?php

session_start();
 
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
?>
<?php 
include "functions.php";
$conn= pdo_connect_mysql();
 


 $id = $_GET['id'];
if(isset($_POST['submit'])){

  $stmt = $conn->prepare('SELECT * FROM images WHERE id = ?');
  $stmt->execute([$_GET['id']]);
  $contact = $stmt->fetch(PDO::FETCH_ASSOC);
  if (!$contact) {
    $query = "INSERT INTO images (id,name,image) VALUES (?,?,?)";
  
  
  $statement = $conn->prepare($query);

   $filename = $_FILES['files']['name'][0];

 
    $target_file = 'upload/'.$filename;

  
    $file_extension = pathinfo($target_file, PATHINFO_EXTENSION);
    $file_extension = strtolower($file_extension);

   
    $valid_extension = array("png","jpeg","jpg");

    if(in_array($file_extension, $valid_extension)){

    
       if(move_uploaded_file($_FILES['files']['tmp_name'][0],$target_file)){

        
	  $statement->execute(array($id,$filename,$target_file));

       }
    }
 
  
  echo "File upload successfully";

  header("location: profile.php");
}
else{

  $stmt = $conn->prepare('DELETE FROM images WHERE id = ?');
  $stmt->execute([$_GET['id']]);
  $query = "INSERT INTO images (id,name,image) VALUES (?,?,?)";
  
  
  $statement = $conn->prepare($query);

   $filename = $_FILES['files']['name'][0];

 
    $target_file = 'upload/'.$filename;

  
    $file_extension = pathinfo($target_file, PATHINFO_EXTENSION);
    $file_extension = strtolower($file_extension);

   
    $valid_extension = array("png","jpeg","jpg");

    if(in_array($file_extension, $valid_extension)){

    
       if(move_uploaded_file($_FILES['files']['tmp_name'][0],$target_file)){

        
	  $statement->execute(array($id,$filename,$target_file));

       }
    }
 
  
  echo "File upload successfully";
  header("location: profile.php");
}


header("location: profile.php");
  
}
?>
  

<?=template_header('Upload File')?>
<!DOCTYPE html>
<html>
<body>
<div class="content">
	<div style="

    margin-top:10%;
    margin-left: 20%;
    border-radius:12px;
    color: black;
    
">
<form method='post' action='' enctype='multipart/form-data'>
  <input type='file' name='files[]' multiple />
  <input type='submit' value='Submit' name='submit' />
</form>


    </div>

</div>
</body>
</html>

<?=template_footer()?>