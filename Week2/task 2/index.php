<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../style.css">
  <title>Simple converter</title>

</head>
<?php
if ($_POST) {
  $firstNumber = $_POST["firstValue"];
  $operation  = $_POST['choice'];
  $result = 0.0;
  switch ($operation) {
    case "kgToPound":
      $result = $firstNumber * 2.20462;
      break;
    case "metreToYard":
      $result = $firstNumber * 1.09361;
      break;
    case "celToFah":
      $result = ($firstNumber * (9 / 5)) + 32;
      break;
    case "pountToKilo":
      $result = $firstNumber * 0.453592;
      break;
    case "yardToMetre":
      $result = $firstNumber * 0.9144;
      break;
    case "FahtoCel":
      $result = ($firstNumber - 32) * (5 / 9);
      break;
  }
}

?>

<body>

  <p class="header">Simple unit Converter </p>
  <div class="mainDiv">
    <form action="" method="post">
      <div class="unitSelect">
        <label for="unitConverter">Select the unit to Convert </label>:</label>
        <select name="choice" class="options">
          <option value="kgToPound">Kilos To Pound </option>
          <option value="metreToYard">Metre To Yard </option>
          <option value="celToFah">Celcius To Fahrenheit </option>
          <option value="pountToKilo">Pound To Kilos </option>
          <option value="yardToMetre">Yard To Metre </option>
          <option value="FahtoCel">Fahrenheit To Celcius </option>
        </select>
      </div>
      <div class="formInput">
        Enter the Value <input class="box1" type="number" step="0.01" name="firstValue" value="0.0"><br>
  
        <input class="btnsubmit" type="submit">
        <br>
        <br>
        <?php
        if ($_POST) {
          switch ($operation) {
            case "kgToPound":
              echo "$firstNumber kilos Converts to  : $result pounds ";
              break;
            case "metreToYard":
              echo "$firstNumber metre Converts to  : $result yard ";
              break;
            case "celToFah":
              echo "$firstNumber  celcius Converts to  : $result  Fahrenheit ";
              break;
            case "pountToKilo":
              echo "$firstNumber pounds Converts to  : $result  kilo ";
              break;
            case "yardToMetre":
              echo "$firstNumber yard Converts to  : $result  metre ";
              break;
            case "FahtoCel":
              echo "$firstNumber  Fahrenheit Converts to  : $result   celcius ";
              break;
          }
        }

        ?>
      </div>



    </form>




  </div>

</body>

</html>